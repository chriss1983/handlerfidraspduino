package com.bqp.startapp;

import com.bqp.utils.MainController;
import com.bqp.utils.SerialConnector;

/**
 * RaspDuino RFID Time Recording
 * 
 * @author Christian Richter Date: 10.06.2015 Version: 1.0
 */

public class TimeRecorder {
	public static void main(String[] args) {
	//	@SuppressWarnings("unused")
		SerialConnector serialConnector = SerialConnector.getInstance();
		
		new MainController();
		
	}
}

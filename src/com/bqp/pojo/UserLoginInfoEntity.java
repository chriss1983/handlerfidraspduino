
package com.bqp.pojo;

import java.util.Date;

public class UserLoginInfoEntity {

	
	private Integer userLoginInfoId;

	private Date startTime;

	private Date endTime;

	private Integer userId;

	public Integer getUserLoginInfoId() {
		return userLoginInfoId;
	}

	public void setUserLoginInfoId(Integer userLoginInfoId) {
		this.userLoginInfoId = userLoginInfoId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserLoginInfo [userLoginInfoId=" + userLoginInfoId + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", userId=" + userId + "]";
	}

}

package com.bqp.view.panels;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.bqp.utils.MainController;
import com.bqp.view.constants.Colors;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class ImageLoader extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String path[] = {"/src/ressources/images/", "/ressources/images/"};
	private int index = 0;

	public ImageLoader() throws IOException {
		
		/** DEBUG*/
		if(MainController.debug) {
			index = 1;
		}
		
		/** Create new JPanel and Add new BorderLayout */
		this.setLayout(new BorderLayout());
		this.setBackground(Colors.bgColor);

		
		/** Get Image */
			URL image = getClass().getResource(path[index] + "bq_Logo_transparent.jpg");
		
		/** Create a Label for Logo and set this to Center */
		JLabel label = new JLabel();
		/** Create new Image Icon */
		label.setIcon(new ImageIcon(image));
		/** set Label to Center */
		label.setHorizontalAlignment(SwingConstants.CENTER);
		/** add Label to This Panel */
		this.add(label, BorderLayout.CENTER);

		// mainPanel.add( label, BorderLayout.NORTH); // Add Logo to
		// JPanel-North

	}
}

package com.bqp.view.panels;

import java.awt.Color;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.view.constants.Colors;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class DigitalClock extends JPanel implements Runnable, Colors {

	private static final long serialVersionUID = 1L;
	private JLabel label;
	private Date date;
	private Font font = new Font("Verdana", Font.PLAIN, 40);
	private Thread thread;

	public DigitalClock() {		
		label = new JLabel();
		label.setFont(font);
		this.setBorder(BorderFactory.createEtchedBorder(Color.BLACK, Color.GRAY)); //(Color.BLACK));
		this.setBackground(bgColor);
		this.setForeground(fgColor);
		this.add(label);
		start();
	}

	private void getDate() {
		SimpleDateFormat formatter = new SimpleDateFormat( 
                "dd.MM.yyyy - HH:mm:ss "); 
        Date currentTime = new Date(); 
  
        label.setText(formatter.format(currentTime));         //2015.06.7 - 21:34:07
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	public void run() {

		while (true) {
			date = new Date();
			getDate();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
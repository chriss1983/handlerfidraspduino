package com.bqp.view.panels;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.bqp.view.InfoPages;
import com.bqp.view.menu.Menus;
import com.bqp.view.menu.view_menu_panels.AbsenceReason;

public class NavigationMenuPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private static NavigationMenuPanel instance;
	
	private Robot r = null;
	public static String reason;
	public static String MENU = "";

	private NavigationMenuPanel() throws IOException {
		this.setLayout(new CardLayout());
		
		try {
			addPanels();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @param panel
	 *            Add a JPanel to this Frame
	 * @param layout
	 *            to Set the Position in this Frame for {@code BorderLayout.NORTH}
	 * @throws InterruptedException 
	 */
	public void addPanels() throws InterruptedException {
		for (int i = 0; i < Menus.menu.length; i++) {
			// logger.debug("Add JPanels : " +
			// InfoPages.PAGES.get(InfoPages.page[i]).getClass().getName());
			// logger.debug("Page String : " + InfoPages.page[i]);
			System.out.println("Add: " + Menus.menu[i]);
			this.add((JPanel) Menus.getMenu().get(Menus.menu[i]), Menus.menu[i]);
		}

		//this.add(pnlCardLayout, BorderLayout.SOUTH);
		
		
	}

	public void setPage(String page) {
		CardLayout cards = (CardLayout) this.getLayout();
		cards.show(this, page);
	}

	public void makeAction(String MENU) {
		Thread t = null;
		try {
			r = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Action: " + MENU);
		if (MENU.equals("BTN_LEFT")) {
			this.setPage("TESTMENU");
		} else if (MENU.equals("BTN_LEFT_CENTER")) {
			this.setPage("HOME");
		} else if (MENU.equals("BTN_CENTER")) {
//			System.out.println("Send \"" + actions[0] + "\" to MainController");
			com.bqp.utils.MainController.viewMenu = "ABSENCEREASON";
		} else if (MENU.equals("BTN_RIGHT_CENTER")) {
		} else if (MENU.equals("BTN_RIGHT")) {
		}
		
	}

	public static NavigationMenuPanel getInstance() {
		if (NavigationMenuPanel.instance == null) {
			try {
				instance = new NavigationMenuPanel();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}

	private void close() {
		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_ALT);
		r.keyRelease(KeyEvent.VK_F4);
	}
	
	private void up() {
		r.keyPress(KeyEvent.VK_UP);
		r.setAutoDelay(100);
		r.keyRelease(KeyEvent.VK_UP);
	}
	
	private void down() {
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
	}
	
	private void left() {
		r.keyPress(KeyEvent.VK_LEFT);
		r.keyRelease(KeyEvent.VK_LEFT);
	}

	private void right() {
		r.keyPress(KeyEvent.VK_RIGHT);
		r.keyRelease(KeyEvent.VK_RIGHT);
	}

	private void select() {
		r.keyPress(KeyEvent.VK_SPACE);
		r.keyRelease(KeyEvent.VK_SPACE);
	}
	
	private void tab() {
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
	}
}

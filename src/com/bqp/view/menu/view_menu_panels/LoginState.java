package com.bqp.view.menu.view_menu_panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.bqp.view.menu.Views;

public class LoginState implements Runnable, Views {
	private final int height = 400, width = 300;
	private String reason = "";
	private String menu;

	public LoginState() {
	}

	public void init() {
		JFrame frame = new JFrame("Login State");
		frame.setSize(new Dimension(width, height));
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);

		JPanel selector = new JPanel(new BorderLayout());
		Label lblInfo = new Label();
		lblInfo.setText("Loginstatus:");
		lblInfo.setFont(new Font("Arial", Font.PLAIN, 40));

		/** Button */
		JButton button = new JButton("W�hlen");
		button.setFont(new Font("Arial", Font.PLAIN, 40));
		button.setBackground(Color.BLUE);
		button.setFocusPainted(true);
		button.setForeground(Color.CYAN);

		UIManager.put("JButton.button", new Color(194, 155, 107));

		/** add Components to Panel */
		selector.add(lblInfo, BorderLayout.NORTH);
		selector.add(button, BorderLayout.CENTER);

		/** add Panel to Frame */
		frame.add(selector);

		frame.pack();
		frame.setVisible(true);
	}

	public String getReason() {
		return reason;
	}

	@Override
	public void run() {
		init();
	}

	@Override
	public void setMenu(String btn) {
		menu = btn;
	}
}

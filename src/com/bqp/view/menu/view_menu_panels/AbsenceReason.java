package com.bqp.view.menu.view_menu_panels;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.bqp.utils.MainController;
import com.bqp.view.menu.Views;
import com.bqp.view.panels.NavigationMenuPanel;

public class AbsenceReason extends JPanel implements Views {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String reason = "";
	private String data;
	private Robot r;
	// private String actions[] = { "BTN_LEFT", "BTN_LEFT_CENTER", "BTN_CENTER",
	// "BTN_CENTER_RIGHT", "BTN_RIGHT", "LOGIN&Christian Richter", "ERROR",
	// "DEFAULT" };

	public static String absenceReason;

	private JLabel lblWelcome, lblText, lblInfo;

	public AbsenceReason() {
		String[] bookTitles = new String[] { "", "working", "pause", "Outside Appointment", "smoking" };
		System.out.println("CALL ME MAYBE");
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		this.setBackground(Color.YELLOW);

		// setLayout(new BorderLayout());

		Box vBox = Box.createVerticalBox();
		Box hBox = Box.createHorizontalBox();

		// lblWelcome = new JLabel("Willkommen", JLabel.CENTER);
		lblText = new JLabel("Abwesenheits Grund: ", JLabel.CENTER);
		lblInfo = new JLabel(absenceReason, JLabel.CENTER);

		// lblWelcome.setFont(new Font("Helvetica", Font.PLAIN, 45));
		setFont(lblText, 40);
		setFont(lblInfo, 40);

		vBox.add(lblText);
		vBox.add(lblInfo);

		vBox.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());

		this.add(hBox, BorderLayout.CENTER);
	}

	private void setFont(JLabel label, int fontSize) {
		label.setFont(new Font("Helvetica", Font.PLAIN, fontSize));
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		absenceReason = reason;
	}

	@Override
	public void setMenu(String btn) {

		Thread t = null;
		try {
			r = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Action: " + btn);
		if (btn.equals("BTN_LEFT")) {
			lblInfo.setText("DAS Ist ein Test");
			com.bqp.controller.GuiController.changeView("ABSENCEREASON");
		} else if (btn.equals("BTN_LEFT_CENTER")) {
//			close();
			//MainController.viewMenu("HOME");
			System.out.println("HIER BIN ICH!!!");
		} else if (btn.equals("BTN_CENTER")) {
		} else if (btn.equals("BTN_RIGHT_CENTER")) {
		} else if (btn.equals("BTN_RIGHT")) {
		}
	}
	
	private void close() {
		r.keyPress(KeyEvent.VK_ALT);
		r.keyPress(KeyEvent.VK_F4);
		r.keyRelease(KeyEvent.VK_ALT);
		r.keyRelease(KeyEvent.VK_F4);
	}
	
}

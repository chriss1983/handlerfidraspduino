package com.bqp.view.menu.view_menu_panels;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.view.menu.Views;

public class TestMenu extends JPanel implements Views {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public TestMenu() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		Box vBox = Box.createHorizontalBox();
		//this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		JButton a = new JButton("TEst");
		JButton b = new JButton("TEst");
		JButton c = new JButton("TEst");
		JButton d = new JButton("TEst");
		JButton e = new JButton("TEst");

		
		vBox.setAlignmentY(CENTER_ALIGNMENT);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(a);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(b);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(c);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(d);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(e);
		vBox.add(Box.createHorizontalGlue());
		
		this.add(vBox);
	}


	@Override
	public void setMenu(String btn) {
		// TODO Auto-generated method stub
		
	}
}

package com.bqp.view.menu.view_menu_panels;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.utils.MainController;
import com.bqp.view.menu.Views;

public class HomeMenu extends JPanel implements Views {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblLeft = new JLabel();
	private JLabel lblLeftCenter = new JLabel();
	private JLabel lblCenter = new JLabel();
	private JLabel lblRightCenter = new JLabel();
	private JLabel lblRight = new JLabel();
	
	private final String path[] = {"/src/ressources/images/", "/ressources/images/"};
	private int index = 0;
	
	public HomeMenu() {
		
		/** DEBUG */
		if(MainController.debug) {
			index = 1;
		}
		
		this.setLayout(new BorderLayout());
		Box vBox = Box.createHorizontalBox();
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		/** Get Image */
		URL left = getClass().getResource(path[index] +  "favs.png");
		URL leftCenter = getClass().getResource(path[index] + "blank.png");
		URL center = getClass().getResource(path[index] + "home.png");
		URL rightCenter = getClass().getResource(path[index] + "up.png");
		URL right = getClass().getResource(path[index] + "down.png");

		/** Create new Image Icon */
		lblLeft.setIcon(new ImageIcon(left));
		
		lblLeftCenter.setIcon(new ImageIcon(leftCenter));
		lblCenter.setIcon(new ImageIcon(center));
		lblRightCenter.setIcon(new ImageIcon(rightCenter));
		lblRight.setIcon(new ImageIcon(right));

		vBox.setAlignmentY(CENTER_ALIGNMENT);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(lblLeft);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(lblLeftCenter);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(lblCenter);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(lblRightCenter);
		vBox.add(Box.createHorizontalGlue());
		vBox.add(lblRight);
		vBox.add(Box.createHorizontalGlue());
		
		this.add(vBox);
	}

	@Override
	public void setMenu(String btn) {
		
	}
}

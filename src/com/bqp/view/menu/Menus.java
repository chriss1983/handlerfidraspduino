package com.bqp.view.menu;

import java.util.HashMap;
import java.util.Map;

import com.bqp.view.menu.view_menu_panels.AbsenceReason;
import com.bqp.view.menu.view_menu_panels.HomeMenu;
import com.bqp.view.menu.view_menu_panels.TestMenu;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public interface Menus {
	public String[] menu = {"HOME", "TESTMENU", "ABSENCEREASON"}; //, "ABSENCEREASON", "LOGIN_STATE"   
	
	public Map<String, Object> BUTTON = getButton();

	public static Map<String, Object> getButton() {
		Map<String, Object> button = new HashMap<String, Object>();
		button.put("BTN_RIGHT", new AbsenceReason());
		return button;
	}
	
	public Map<String, Object> MENUVIEW = getMenu(); 
	
	public static Map<String, Object> getMenu() {
		Map<String, Object> menus = new HashMap<String, Object>();
		menus.put("TESTMENU", new TestMenu());
		menus.put("HOME", new HomeMenu());
		menus.put("ABSENCEREASON", new AbsenceReason());
	//	menus.put("LOGIN_STATE", new LoginState());
		return menus;
	}
	
	
	

}

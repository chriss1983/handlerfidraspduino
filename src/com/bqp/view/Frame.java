package com.bqp.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.bqp.view.constants.Colors;
import com.bqp.view.menu.Menus;
import com.bqp.view.menu.view_menu_panels.AbsenceReason;
import com.bqp.view.panels.DigitalClock;
import com.bqp.view.panels.ImageLoader;
import com.bqp.view.panels.NavigationMenuPanel;
import com.bqp.view.view_center_panels.LoginView;
import com.bqp.view.view_center_panels.LogoutView;

/**
 * RaspDuino RFID Time Recording
 * 
 * @author Christian Richter Date: 10.06.2015 Version: 1.0
 */

public class Frame extends JFrame implements Colors {
	// final static Logger logger = Logger.getLogger(Frame.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** Frame Size */
	private int width, height;

	private JPanel mainPanel;
	private JPanel southPanel;
	private JPanel cardLayout;
	private JPanel dataContainer = new JPanel();

	public static String settingData;
	private DigitalClock clock;

	InfoPages infoPages;

	// private static String rfid;

	/**
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	// ArduinoSerial arduinoSerial = new ArduinoSerial();

	public Frame(Boolean decorated, Boolean resizable, int width, int height) throws IOException {

		/** Framesize */
		this.width = width;
		this.height = height;

		/** Is Decorated? */
		this.setUndecorated(decorated);

		/** Resizable? */
		this.setResizable(resizable);

		/** Close and Clean when close this Frame */
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/** Initialize the Gui */
		initGui();

	}

	/**
	 * Go into Fullscreen
	 * 
	 * @param width
	 * @param heigt
	 * @param colourDeep
	 * @param refreshRate
	 */
	@SuppressWarnings("unused")
	private void setGraphicMode(int width, int heigt, int colourDeep, int refreshRate) {
		setSize(Toolkit.getDefaultToolkit().getScreenSize()); // Get Screensize
		GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
		device.setFullScreenWindow(this);
		device.setDisplayMode(new DisplayMode(width, heigt, colourDeep, refreshRate));
	}

	/**
	 * Hide the Cursor
	 */
	public void hideCursor() {
		// logger.info("Hide Cursor");
		this.setCursor(this.getToolkit().createCustomCursor(new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "null"));
	}

	/**
	 * init the Components
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void initGui() throws IOException {
		// logger.info("Init GUI");
		this.setUndecorated(true); // No Decorations
		this.setSize(width, height); // Set Size
		/** Set Frame to Monitor Center */
		this.setLocationRelativeTo(null);

		/** Create a Main-Panel for any Components */
		mainPanel = new JPanel(new BorderLayout());
		southPanel = new JPanel(new BorderLayout());

		/** Add a new CardLayout to mainPanel */
		cardLayout = new JPanel(new CardLayout());

		/** set Forground and Background-Color to White */
		mainPanel.setBackground(bgColor); // set BackgroundColor
		mainPanel.setForeground(fgColor); // set ForgroundColor

		/** Create Clock Panel */
		clock = new DigitalClock(); // Create a Clock Object

		clock.start(); // Start the Clock Thread

		/** Add Default Panels */

		/** North Content */
		this.mainPanel.add(new ImageLoader(), BorderLayout.NORTH);
		this.mainPanel.add(dataContainer);

		/** South Content */
		this.southPanel.add(clock, BorderLayout.NORTH);

		southPanel.add(NavigationMenuPanel.getInstance(), BorderLayout.SOUTH);
		NavigationMenuPanel.getInstance().setPage("HOME");
		this.getContentPane().add(southPanel, BorderLayout.SOUTH);

	}

	/**
	 * Make Frame Visible
	 */
	public void showView() {
		this.getContentPane().add(mainPanel);
		this.setVisible(true);
	}

	/**
	 * 
	 * @param panel
	 *            Add a JPanel to this Frame
	 * @param layout
	 *            to Set the Position in this Frame {@code BorderLayout.NORTH}
	 */
	public void addPanels() {

		for (int i = 0; i < InfoPages.page.length; i++) {
			// logger.debug("Add JPanels : " +
			// InfoPages.PAGES.get(InfoPages.page[i]).getClass().getName());
			// logger.debug("Page String : " + InfoPages.page[i]);
			System.out.println("Add: " + InfoPages.page[i]);
			cardLayout.add((JPanel) InfoPages.PAGES.get(InfoPages.page[i]), InfoPages.page[i]);
		}

		for (int i = 0; i < Menus.menu.length; i++) {
			// logger.debug("Add JPanels : " +
			// InfoPages.PAGES.get(InfoPages.page[i]).getClass().getName());
			// logger.debug("Page String : " + InfoPages.page[i]);
			System.out.println("Add: " + Menus.menu[i]);
			cardLayout.add((JPanel) Menus.MENUVIEW.get(Menus.menu[i]), Menus.menu[i]);
		}

		mainPanel.add(cardLayout, BorderLayout.CENTER);

	}

	public void setPage(String page) {
		// logger.debug("Called Page : " + page);

		if (page.contains("&")) {
			/** Split by '&' */
			String cmd[] = page.split("&");
			/** Save String before '&' to 'page' String */
			page = cmd[0];
			/** Save String after '&' to 'userName' */
			settingData = cmd[1];
			// logger.info("Call Page : " + page + "\nUsername : " + userName);
			if (InfoPages.PAGES.get(page) instanceof LoginView) {
				((LoginView) InfoPages.PAGES.get(page)).setUserName(settingData);
			} else if (InfoPages.PAGES.get(page) instanceof LogoutView) {
				((LogoutView) InfoPages.PAGES.get(page)).setUserName(settingData);
			}
		}

		CardLayout cards = (CardLayout) cardLayout.getLayout();
		cards.show(cardLayout, page);
		// mainPanel.repaint();
	}

}

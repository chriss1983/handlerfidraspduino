package com.bqp.view;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import com.bqp.view.view_center_panels.DefaultView;
import com.bqp.view.view_center_panels.ErrorView;
import com.bqp.view.view_center_panels.FailView;
import com.bqp.view.view_center_panels.InfoView;
import com.bqp.view.view_center_panels.InternalErrorView;
import com.bqp.view.view_center_panels.LoginView;
import com.bqp.view.view_center_panels.LogoutView;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public interface InfoPages {
	public String[] page = { "DEFAULT", "LOGIN", "FAIL", "ERROR", "LOGOUT", "INTERNAL_ERROR", "INFO" };

	public Map<String, Object> PAGES = getPages();

	public static Map<String, Object> getPages() {
		Map<String, Object> pages = new HashMap<String, Object>();
		pages.put("DEFAULT", new DefaultView());
		pages.put("LOGIN", new LoginView());
		pages.put("FAIL", new FailView());
		pages.put("ERROR", new ErrorView());
		pages.put("LOGOUT", new LogoutView());
		pages.put("INTERNAL_ERROR", new InternalErrorView());
		pages.put("INFO", new InfoView());
		return pages;
	}

}

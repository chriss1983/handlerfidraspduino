package com.bqp.view.view_center_panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.bqp.view.Frame;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class ErrorView extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel lblName;
	JLabel lblInfo;

	
	public ErrorView() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		this.setBackground(Color.RED);
		
		//setLayout(new BorderLayout());
		
		Box vBox = Box.createVerticalBox();
		Box hBox = Box.createHorizontalBox();
		
		//lblWelcome = new JLabel("Willkommen", JLabel.CENTER);
		lblName = new JLabel("FEHLER, diese Karte ist", JLabel.CENTER);
		lblInfo = new JLabel("dem System nicht bekannt", JLabel.CENTER);
		
		//lblWelcome.setFont(new Font("Helvetica", Font.PLAIN, 45));
		lblName.setFont(new Font("Helvetica", Font.PLAIN, 40));
		lblInfo.setFont(new Font("Helvetica", Font.PLAIN, 40));
		

		
		//vBox.add(lblWelcome);
		
		vBox.add(lblName);
		
		vBox.add(lblInfo);
		

		
		vBox.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());
		
		this.add(hBox, BorderLayout.CENTER);
	}
	
	public JPanel getPnl() {
		return this;
	}
}

package com.bqp.view.view_center_panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bqp.utils.MainController;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class LoginView extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String name;
	public static String time;
	
	JLabel lblWelcome, lblName, lblInfo;

	
	public LoginView() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		this.setBackground(Color.GREEN);
	
		//setLayout(new BorderLayout());
		
		Box vBox = Box.createVerticalBox();
		Box hBox = Box.createHorizontalBox();
		
		//lblWelcome = new JLabel("Willkommen", JLabel.CENTER);
		lblName = new JLabel("Hallo " + name, JLabel.CENTER);
		lblInfo = new JLabel("Sie wurden erfolgreich angemeldet", JLabel.CENTER);
		
		//lblWelcome.setFont(new Font("Helvetica", Font.PLAIN, 45));
		setFont(lblName, 40);
		setFont(lblInfo, 40);
		
		vBox.add(lblName);
		vBox.add(lblInfo);
		
		vBox.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());
		
		this.add(hBox, BorderLayout.CENTER);
	}
	
	/**
	 * 
	 * @param label set the needed Font to the Label
	 */
	private void setFont(JLabel label, int fontSize) {
		label.setFont(new Font("Helvetica", Font.PLAIN, fontSize));
	}
	
	public void setUserName(String name) {
		lblName.setText("Hallo " + name); 
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JPanel getPnl() {
		return this;
	}
}

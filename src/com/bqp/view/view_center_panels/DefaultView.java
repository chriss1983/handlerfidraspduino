package com.bqp.view.view_center_panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class DefaultView extends JPanel {
	JLabel lblWelcome;
	JLabel lblInfo;
	public DefaultView() {

		setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		
		Box vBox = Box.createVerticalBox();
		Box hBox = Box.createHorizontalBox();

		//lblWelcome = new JLabel("BQ-Projects Zeiterfassung");
		//lblWelcome.setFont(new Font("Arial", Font.PLAIN, 50));
		
		lblInfo = new JLabel("Zur Zeiterfassung RFID-Tag auflegen", JLabel.CENTER);
		lblInfo.setFont(new Font("Arial", Font.PLAIN, 40));
		//lblInfo.setForeground(Color.BLUE);
		
		
		//vBox.add(lblWelcome);
		
		vBox.add(lblInfo);
		//vBox.add(Box.createHorizontalGlue());
		vBox.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());

		this.add(hBox, BorderLayout.CENTER);
	}
}

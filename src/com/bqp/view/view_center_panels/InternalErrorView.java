package com.bqp.view.view_center_panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.bqp.view.Frame;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class InternalErrorView extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel lblInfo0;
	JLabel lblInfo1;
	JLabel lblStars0;
	JLabel lblStars1;

	
	public InternalErrorView() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		this.setBackground(Color.RED);
		
		//setLayout(new BorderLayout());
		
		Box vBox = Box.createVerticalBox();
		Box hBox = Box.createHorizontalBox();
		
		//lblWelcome = new JLabel("Willkommen", JLabel.CENTER);
		lblStars0 = new JLabel("________________________________________", JLabel.CENTER);
		lblStars1 = new JLabel("________________________________________", JLabel.CENTER);
		lblInfo0 = new JLabel("ACHTUNG, Es ist ein Interner Fehler", JLabel.CENTER);
		lblInfo1 = new JLabel("aufgetreten, wenden Sie sich an den Admin", JLabel.CENTER);
		
		//lblWelcome.setFont(new Font("Helvetica", Font.PLAIN, 45));
		lblStars0.setFont(new Font("Helvetica", Font.PLAIN, 40));
		lblStars1.setFont(new Font("Helvetica", Font.PLAIN, 40));
		lblInfo0.setFont(new Font("Helvetica", Font.PLAIN, 40));
		lblInfo1.setFont(new Font("Helvetica", Font.PLAIN, 40));
		
		//vBox.add(lblWelcome);
		vBox.add(lblStars0);
		vBox.add(lblInfo0);
		
		vBox.add(lblInfo1);
		vBox.add(lblStars1);

		
		vBox.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());
		
		this.add(hBox, BorderLayout.CENTER);
	}
	
	public JPanel getPnl() {
		return this;
	}
}

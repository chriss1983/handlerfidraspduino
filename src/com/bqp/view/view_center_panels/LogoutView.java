package com.bqp.view.view_center_panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public class LogoutView extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	public static String name;
	
	JLabel lblWelcome;
	JLabel lblName;
	JLabel lblInfo;

	
	public LogoutView() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLoweredBevelBorder());
		this.setBackground(Color.GREEN);
	
		
		//setLayout(new BorderLayout());
		
		Box vBox = Box.createVerticalBox();
		Box hBox = Box.createHorizontalBox();
		
		//lblWelcome = new JLabel("Willkommen", JLabel.CENTER);
		lblName = new JLabel("Hallo " + name, JLabel.CENTER);
		lblInfo = new JLabel("Sie wurden erfolgreicht abgemeldet", JLabel.CENTER);
		
		//lblWelcome.setFont(new Font("Helvetica", Font.PLAIN, 45));
		lblName.setFont(new Font("Helvetica", Font.PLAIN, 40));
		lblInfo.setFont(new Font("Helvetica", Font.PLAIN, 40));

		
		//vBox.add(lblWelcome);
		
		vBox.add(lblName);
		
		vBox.add(lblInfo);
		

		
		vBox.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		hBox.add(Box.createHorizontalGlue());
		hBox.add(vBox);
		hBox.add(Box.createHorizontalGlue());
		
		this.add(hBox, BorderLayout.CENTER);
	}
	
	public void setUserName(String name) {
		lblName.setText("Hallo " + name); 
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JPanel getPnl() {
		return this;
	}
}

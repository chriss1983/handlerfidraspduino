package com.bqp.view.view_center_panels;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public interface Views {
	DefaultView DEFAULT = new DefaultView();
	LoginView SUCCSESS = new LoginView(); 
	String FAIL = "failView";
	String ERROR = "errorView";
}

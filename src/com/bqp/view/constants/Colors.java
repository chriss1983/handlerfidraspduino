package com.bqp.view.constants;

import java.awt.Color;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */

public interface Colors {
	Color bgColor = Color.WHITE;
	Color fgColor = Color.WHITE;
	
	Color success = Color.GREEN;
	Color fail = Color.YELLOW;
	Color error = Color.RED;
}

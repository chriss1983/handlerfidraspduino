
package com.bqp.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;

import com.bqp.pojo.RequestObj;
import com.bqp.pojo.UserDetailsEntity;
import com.bqp.pojo.UserLoginInfoEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class AthenaServerConnector {

	private static final String ATHENA_DATA_URL = "http://192.168.151.112:8080/Ennio-Client/timesheet/dataProvider2";
	// redeyed.de - 37.201.45.81
	// Home: 192.168.1.27
	// 192.168.151.112
	protected static Gson GSON = new GsonBuilder().create();

	
	public static JSONObject getData(RequestObj requestObj) {
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(ATHENA_DATA_URL);
			StringEntity input = new StringEntity(GSON.toJson(requestObj));
			input.setContentType("application/json");
			httpPost.setEntity(input);
			CloseableHttpResponse response2 = httpclient.execute(httpPost);
			return new JSONObject(getString(response2));
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return new JSONObject();
	}

	public static String getString(CloseableHttpResponse response1) throws UnsupportedOperationException, IOException {
		String response = "";

		System.out.println(response1);
		InputStream content = response1.getEntity().getContent();

		BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
		String s = "";
		while ((s = buffer.readLine()) != null) {
			response += s;
		}
		System.out.println(response);
		return response;
	}

	public static void main(String[] args) throws JsonSyntaxException, JSONException {
		RequestObj requestObj = new RequestObj();
		requestObj.setRequestType("RFID_VALIDATE");
		Map<String, Object> params = new HashMap<String, Object>();
		//params.put("RFID", "MYTEST_RFIDs");
		requestObj.setParams(params);
		JSONObject uiObj = AthenaServerConnector.getData(requestObj);
		MainController.userDetailsEntity = GSON.fromJson(uiObj.getString("USER_DETAILS"), UserDetailsEntity.class);
		MainController.userLoginInfoEntity = GSON.fromJson(uiObj.getString("USER_LOGIN_INFO"), UserLoginInfoEntity.class);

//		System.out.println(userDetailsEntity);
	}
}
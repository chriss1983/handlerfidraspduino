package com.bqp.utils;

import java.util.HashMap;
import java.util.Map;

import com.bqp.controller.GuiController;
import com.bqp.pojo.UserDetailsEntity;
import com.bqp.pojo.UserLoginInfoEntity;
import com.bqp.view.menu.Menus;
import com.bqp.view.menu.Views;
import com.bqp.view.menu.view_menu_panels.AbsenceReason;
import com.bqp.view.panels.NavigationMenuPanel;

public class MainController {

	public static UserDetailsEntity userDetailsEntity;
	public static UserLoginInfoEntity userLoginInfoEntity;
	public static boolean debug = false;
	public static boolean showGui = true;
	public static String viewType = "";
	public static String viewMenu = "";

	public static String button = "";
	public static String reason = "work";

	public static Map<String, Object> userPojos = new HashMap<String, Object>();

	public MainController() {
		try {
			GuiController guiController = new GuiController();
			/** Start the Gui, when the Varriable is true - ONLY DEBUGGING! */
			if (showGui) {
				guiController.createGui();
			}

			System.out.println("Start While Loop");
			while (true) {
				/*
				 * if (!viewMenu.equals("")) { showMenu(viewMenu); }
				 */
				if (!button.equals("")) {
					/** Show Pressed Button */
					System.out.println(button);

					// GuiController.buttonName = button;
					// NavigationPanel.getInstance().makeAction(viewMenu);

					button = "";
				}

				if (!viewType.equals("")) {
					viewType = viewType.replace("\"", "");
					System.out.println(viewType);

					if (viewType.equals("LOGIN") | viewType.equals("LOGOUT") & userDetailsEntity != null) {
						System.out.println("COMMAND = " + viewType + "&" + userDetailsEntity.getFirstName() + " " +  userDetailsEntity.getLastName());
						GuiController.changeView(viewType + "&" + userDetailsEntity.getFirstName() + " " + userDetailsEntity.getLastName());

						viewType = "";
						userDetailsEntity = null;
						// userLoginInfoEntity = null;
					} else if (viewType.equals("ERROR")) {
						GuiController.changeView(viewType);
						viewType = "";
					}
					Thread.sleep(3000);
					GuiController.changeView("DEFAULT");

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showMenu(String menu) {
		System.out.println("Call With: " + menu);
		for (int i = 0; i < Menus.menu.length; i++) {
			System.out.println("VIEWMENU " + menu);

			Views v = (Views) Menus.MENUVIEW.get(menu);
			if (v != null) {
				System.out.println("Menu is : " + v.getClass().getName());
				v.setMenu(button);
			}
			// NavigationPanel.getInstance().makeAction(string);
		}
	}
}

package com.bqp.controller;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import com.bqp.view.Frame;
import com.bqp.view.InfoPages;
import com.bqp.view.menu.Menus;
import com.bqp.view.view_center_panels.DefaultView;
import com.bqp.view.view_center_panels.LoginView;

/**
 * RaspDuino RFID Time Recording
 * 
 * @author Christian Richter
 * @since Date: 10.06.2015
 * @version 1.0
 * 
 *          Last Edit by: Christian Richter 29.07.2015
 *          Reason: 
 *          -	Added the option to change the menu
 *          -	Add detailed comments
 *          
 * 
 *          The Method checks the incoming string after the '&' character and
 *          separates it. The separator is used to send a command for the
 *          display and the name of the user. The area in front of the separator
 *          is to be set and the display area after that to be set name (if
 *          available) The display is set dynamically and can be easily extended
 *          without having to change this method. should not be given separator,
 *          so this is passed without verification, - provided that it is
 *          specified in the interface 'InfoPages' - passed.
 * 
 * 
 * 
 * 
 * 
 */
public class GuiController {
	private static Frame rfidFrame;
	public static String viewMode = "DEFAULT";

	/**
	 * Method to Build a Frame
	 * 
	 * @throws IOException
	 */
	public void createGui() throws IOException {
		/** Create Main Frame */
		rfidFrame = new Frame(false, false, 800, 450);

		/** Hide Cursor from UI */
		rfidFrame.hideCursor();

		/** North Content */
		rfidFrame.addPanels();

		/** South Content */
		rfidFrame.showView();
	}

	/**
	 * 
	 * @param viewMode
	 *            - The page to be displayed
	 */
	public static void changeView(String viewMode) {
		System.out.println("VIEWMODE: " + viewMode);

		/**
		 * Split the incoming String to 'Command' and 'User Firstname -
		 * Lastname'
		 */
		String cmd = null;
		int splitChar = viewMode.indexOf("&");
		if (splitChar != -1) {
			cmd = viewMode.substring(0, splitChar);
		}

		/** constitutes part of the information view dynamically */
		for (int i = 0; i < InfoPages.PAGES.size(); i++) {
			if (viewMode.equals(InfoPages.page[i])) {
				rfidFrame.setPage(InfoPages.page[i]);
			} else if (cmd != null && cmd.equals("LOGIN") || cmd != null && cmd.equals("LOGOUT")) {
				rfidFrame.setPage(viewMode);
			}
		}

		/** sets the pages of the menu dynamically */
		for (int i = 0; i < Menus.menu.length; i++) {
			if (viewMode.equals(Menus.menu[i])) {
				rfidFrame.setPage(viewMode);
			}
		}
	}
}

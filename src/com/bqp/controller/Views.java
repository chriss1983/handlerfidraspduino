package com.bqp.controller;

/**
 * RaspDuino RFID Time Recording
 * @author Christian Richter
 * Date: 10.06.2015
 * Version: 1.0
 */


public enum Views {
	DEFAULT_VIEW, SUCCSESS_VIEW, FAIL_VIEW, ERROR_VIEW
}

package test.keysimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;

import com.bqp.view.Frame;
import com.bqp.view.menu.Menus;
import com.bqp.view.menu.view_menu_panels.AbsenceReason;

public class ButtonListener implements ActionListener {
	String buttonNames[];
	private Map<String, JButton> buttons;
	private String actions[] = { "BTN_LEFT", "BTN_LEFT_CENTER", "BTN_CENTER", "BTN_CENTER_RIGHT", "BTN_RIGHT", "LOGIN&Christian Richter", "ERROR", "DEFAULT" };

	public static String button = "";

	public ButtonListener(String buttonNames[], Map<String, JButton> buttons) {
		this.buttonNames = buttonNames;
		this.buttons = buttons;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		/*
		 * BTN_LEFT
		 */
		if (src == buttons.get(buttonNames[0])) {
//			com.bqp.utils.MainController.button = "ABSENCEREASON";
			System.out.println("Send \"" + actions[0] + "\" to MainController");
			com.bqp.utils.MainController.viewMenu = "ABSENCEREASON";
//			AbsenceReason.absenceReason = "Smoke";
//			AbsenceReason absenceReason = (AbsenceReason) Menus.MENUVIEW.get("ABSENCEREASON");
//			absenceReason.setReason("Outside Appointment");
//			com.bqp.controller.GuiController.changeView("ABSENCEREASON");
//			com.bqp.utils.MainController.change(actions[0]);
		} else if (src == buttons.get(buttonNames[1])) {
			System.out.println("Send \"" + actions[1] + "\" to MainController");
			/*
			 * BTN_LEFT_CENTER
			 */
		} else if (src == buttons.get(buttonNames[2])) {
//			System.out.println("Send \"" + actions[2] + "\" to MainController");
//			com.bqp.utils.MainController.showMenu(actions[1]);
			com.bqp.controller.GuiController.changeView("DEFAULT");
			/*
			 * BTN_CENTER
			 */
		} else if (src == buttons.get(buttonNames[3])) {
			System.out.println("Send \"" + actions[3] + "\" to MainController");
			com.bqp.utils.MainController.showMenu(actions[2]);
			/*
			 * BTN_CENTER_RIGHT
			 */
		} else if (src == buttons.get(buttonNames[4])) {
			System.out.println("Send \"" + actions[4] + "\" to MainController");
//			com.bqp.utils.MainController.showMenu(actions[4]);
			/*
			 * BTN_RIGHT
			 */
		} else if (src == buttons.get(buttonNames[5])) {
			System.out.println("Send \"" + actions[5] + "\" to MainController");
			com.bqp.controller.GuiController.changeView(actions[5]);
			/*
			 * ERROR
			 */
		} else if (src == buttons.get(buttonNames[6])) {
			System.out.println("Send \"" + actions[6] + "\" to MainController");
			com.bqp.controller.GuiController.changeView(actions[6]);
			/*
			 * 7. Button
			 */
		} else if (src == buttons.get(buttonNames[7])) {
			System.out.println("Send \"" + actions[7] + "\" to MainController");
			com.bqp.utils.MainController.showMenu(actions[7]);
			com.bqp.controller.GuiController.changeView(actions[7]);
		}
	}

	public void addListener() {
		for (int i = 0; i < buttonNames.length; i++) {
			// buttons.get(buttonNames[i]).setActionCommand(actions[i]);
			buttons.get(buttonNames[i]).addActionListener(this);
		}
	}

}

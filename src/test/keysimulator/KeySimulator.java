package test.keysimulator;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class KeySimulator extends JFrame implements Runnable {
	private JPanel panel;
	
	private String buttonNames[] = { "BTN_LEFT", "BTN_CENTER_LEFT", "BTN_CENTER", "BTN_CENTER_RIGHT", "BTN_RIGHT", "LOGIN&Christian Richter", "ERROR", "DEFAULT" };
	
	public KeySimulator() {
		initGui();
		
	}
	
	private void initGui() {
		this.setTitle("Test Buttons");
		this.setSize(300, 300);
		this.setLocationRelativeTo(null);
		this.getContentPane().add(getPanles());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	private Component getPanles() {
		JPanel p = new JPanel();
		GridLayout gl = new GridLayout(0,1);
        gl.setVgap(10);
        p.setLayout(gl);
        for(int i = 0; i < buttonNames.length; i++) {
        	p.add(getButtons().get(buttonNames[i]));
        }
      
		return p;
	}
	
	private Map<String, JButton> getButtons() {
		Map<String, JButton> buttons = new HashMap<String, JButton>();
		ButtonListener listener = new ButtonListener(buttonNames, buttons);
		for(int i = 0; i < buttonNames.length; i++) {
			 buttons.put(buttonNames[i], new JButton(buttonNames[i]));
			 buttons.get(buttonNames[i]).addActionListener(listener);
			 buttons.get(buttonNames[i]).setFocusPainted(true);
			 buttons.get(buttonNames[i]).setBackground(Color.CYAN);
			 
		}
		
		return buttons;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
	

}
